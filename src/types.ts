interface IUiState {
  loading: boolean;
}
interface IAction {
  type: string;
  payload?: any;
  meta?: any;
}
