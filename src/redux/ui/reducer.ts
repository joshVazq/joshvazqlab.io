import { createReducer } from '../utils';

import { SET_LOADER } from './actions';

const initState: IUiState = {
  loading: false
};

export const uiReducer = createReducer(initState, {
  [SET_LOADER]: (state: IUiState, action: IAction): IUiState => {
    return { ...state, loading: action.payload };
  }
});
