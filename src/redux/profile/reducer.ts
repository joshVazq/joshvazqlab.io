import { createReducer } from '../utils';
import { SET_PROFILE } from './actions';

const initState: any = null;

export const profileReducer = createReducer(initState, {
  [SET_PROFILE]: (_: any, action: IAction) => {
    //console.log(action.payload);
    //console.log(JSON.stringify(action.payload));
    return action.payload;
  }
});
