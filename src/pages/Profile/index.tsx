import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Profile } from '../../model/profile';
import { fetchProfile } from '../../redux/profile/actions';
import { getProfile } from '../../redux/profile/selectors';
import { WorkBlock } from './WorkBlock';
import { EducationBlock } from './EducationBlock';
import { SkillsBlock } from './SkillsBlock';
import { PersonalInfo } from './PersonalInfo';
import { ContactBlock } from './ContactBlock';

type Props = {
  profile: Profile;
  fetchProfile: Function;
};
const buttonCss = 'sm:text link mh3 p-2 br3 m-2  white bg-teal';
export class ProfilePage extends Component<Props> {
  componentWillMount() {
    this.props.fetchProfile('2x03PVsba4qWSom0A8aYMq');
  }

  render() {
    const profile = this.props.profile;
    if (profile) {
      return (
        <div className="flex flex-col text-gray-600" /* style={{ maxWidth: '1400px' }} */>
          <PersonalInfo profile={profile} />
          <div className="bg-ecru-white px-4">
            <h2 className="my-3 text-center text-5xl font-hairline teal">Experiences</h2>
            <WorkBlock experience={profile.experience} />
            <EducationBlock education={profile.education} />
          </div>
          <div className="bg-white px-4">
            <SkillsBlock skills={profile.skills} />
            <div className="text-center py-4">
              <p className="italic text-gray-500">This project is build with ReactJS and Redux.</p>
              <div className="flex flex-wrap justify-center">
                <a
                  href="https://gitlab.com/joshVazq/joshVazq.gitlab.io"
                  className={buttonCss}
                  target="_blank"
                  rel="noopener noreferrer">
                  See project on Gitlab
                </a>
                <a
                  href="/coverage/"
                  className={buttonCss}
                  target="_blank"
                  rel="noopener noreferrer">
                  Check the Coverage
                </a>
              </div>
            </div>
          </div>
          <div className="bg-gray-600 text-white">
            <ContactBlock />
          </div>
        </div>
      );
    }
    return null;
  }
}
export const mapStateToProps = (state: any) => ({ profile: getProfile(state) });

export default connect(
  mapStateToProps,
  { fetchProfile }
)(ProfilePage);
