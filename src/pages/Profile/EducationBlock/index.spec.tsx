import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import { EducationBlock } from './index';
import { Education } from '../../../model/education';
import { Period } from '../../../model/period';

describe('EducationBlock', () => {
  it('should render without crashing', () => {
    const edu = new Education('');
    edu.dates = new Period(new Date());
    const education = [edu];
    shallow(<EducationBlock education={education} />);
  });
  it('should render description', () => {
    const edu = new Education('');
    edu.dates = new Period(new Date());
    edu.description = 'description';
    const education = [edu];
    shallow(<EducationBlock education={education} />);
  });
});
