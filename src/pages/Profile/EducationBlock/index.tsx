import React from 'react';
import Markdown from 'markdown-to-jsx';
import { Education } from '../../../model/education';
import { sortByFromDesc } from '../../../utils';

type Props = {
  education: Education[];
};
function renderEducationList(list: Education[]) {
  return list.sort(sortByFromDesc).map((education: Education) => (
    <div key={education.id} className="mb-8 flex flex-col lg:flex-row lg:justify-between">
      <div className="lg:w-1/3">
        <h4 className="font-bold text-2xl lg:mt-8 text-gray-700">{education.degree}</h4>
        <h4 className="font-normal">
          <i className="fa fa-calendar  mr-4" />
          {education.dates.toString()}
        </h4>
      </div>
      <div className="lg:w-3/5">
        <h4 className="font-bold mt-4 lg:mt-8">
          <i className="fa fa-building  mr-4" />
          {education.school}
        </h4>
        {education.description && (
          <Markdown {...{ className: 'block list' }} /* options={markDownOptions} */>
            {education.description}
          </Markdown>
        )}
      </div>
    </div>
  ));
}
export const EducationBlock = ({ education }: Props) => (
  <div className="my-4 px-16 lg:mx-auto lg:w-11/12">
    <h3 className="font-normal text-2xl my-3 font-hairline teal">
      <i className="fa fa-graduation-cap  mr-4  teal" />
      Education
    </h3>
    {renderEducationList(education)}
  </div>
);
