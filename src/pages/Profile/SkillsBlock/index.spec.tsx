import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import { SkillsBlock, getStars } from './index';
import { Skill } from '../../../model/skill';

describe('SkillsBlock', () => {
  it('should render without crashing', () => {
    const skill = new Skill('', 'test', 100);
    const skills = [skill];
    shallow(<SkillsBlock skills={skills} />);
  });

  describe('getStars', () => {
    it('should return 1 star filled', () => {
      const result = getStars(20);
      expect(result).toEqual([0, 2, 2, 2, 2]);
    });
    it('should return 5 stars filled', () => {
      const result = getStars(100);
      expect(result).toEqual([0, 0, 0, 0, 0]);
    });
    it('should return 4 and a half stars filled', () => {
      const result = getStars(90);
      expect(result).toEqual([0, 0, 0, 0, 1]);
    });
    it('should return 1 and a half stars filled', () => {
      const result = getStars(30);
      expect(result).toEqual([0, 1, 2, 2, 2]);
    });
  });
});
