import React from 'react';
import { Skill } from '../../../model/skill';
import './style.css';
import { sortSkill } from '../../../utils';

type Props = {
  skills: Skill[];
};

const starClasses = ['fa fa-star', 'fa fa-star-half-alt', 'far fa-star'];
/* 
const renderPercentage = (name, level) => {
  const percentage = `${level}%`;

  return (
    <div>
      <p>{name}</p>,
      <div className="lg:bg-light-gray br-pill ">
        <div className="px-4 py-1 text-center text-xs br-pill bg-teal" style={{ width: percentage }}>
          {percentage}
        </div>
      </div>
    </div>
  );
}; */

const levelToStars = (level: number) => Math.round(((level * 5) / 100) * 2) / 2; //(level * 5) / 100;
export const getStars = (level: number) => {
  const scale5 = levelToStars(level);
  const filled = Math.floor(scale5);
  let empty = Math.floor(5 - filled);
  let half: number[] = [];
  if (scale5 % 1 === 0.5) {
    half = [1];
    empty--;
  }
  return [...new Array(filled).fill(0), ...half, ...new Array(empty).fill(2)];
};
const renderStar = (id: string, name: string, level: number) => {
  const stars = getStars(level);
  return (
    <div className="flex xl:text justify-between items-center">
      {/*  text-4xl-ns */}
      <span className="dib font-thin">{name}</span>
      <div className="font-thin w-32 flex justify-around">
        {/*  w-64-ns */}
        {stars.map((type, i) => (
          <i key={'rating-' + id + '-' + i} className={'teal ' + starClasses[type]} />
        ))}
      </div>
    </div>
  );
};

const renderSkill = (skill: Skill) => {
  /* lg:fl lg:w-64 */
  return (
    <li className="lg:hover-bg-light-gray p-1" key={skill.id}>
      {renderStar(skill.id, skill.name, skill.level)}
      {/* renderPercentage(skill.name, skill.level) */}
    </li>
  );
};
/* fl shadow-1 mv3  bg-white p-4
 */
export const SkillsBlock = ({ skills }: Props) => (
  <div className="my-4 px-16  lg:mx-auto lg:w-11/12" id="skills">
    <h3 className="text-2xl  my-3 font-hairline teal">
      <i className="fa fa-asterisk  mr-4  teal" />
      Skills
    </h3>
    <ul className="list">{skills.sort(sortSkill).map(renderSkill)}</ul>
    <br />
  </div>
);
