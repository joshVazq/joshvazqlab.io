import React from 'react';

export const ContactBlock = () => (
  <div id="contact" className="text-center lg:w-1/2 md:w-4/5 mx-auto">
    <h2 className="my-3 text-5xl font-hairline mb0">Contact</h2>
    <p className="text-2xl p-4 font-hairline">
      "I don't use a lot social media, usually read, rarely post, but if you ping me I'll answer
      you."
    </p>
    <ul className="list flex justify-around text-5xl">
      <li data-aos="slide-right">
        <a href="https://twitter.com/JoshVazq" target="_blank" rel="noopener noreferrer">
          <i className="fab fa-twitter " />
        </a>
      </li>
      <li data-aos="slide-right">
        {' '}
        <a
          href="https://www.linkedin.com/in/josué-vázquez-rendo-5a303794/"
          target="_blank"
          rel="noopener noreferrer">
          <i className="fab fa-linkedin " />
        </a>
      </li>
      <li data-aos="zoom-in">
        <a href="https://github.com/JoshVazq" target="_blank" rel="noopener noreferrer">
          <i className="fab fa-github " />
        </a>
      </li>
      <li data-aos="slide-left">
        <a href="https://gitlab.com/joshVazq" target="_blank" rel="noopener noreferrer">
          <i className="fab fa-gitlab " />
        </a>
      </li>
      <li data-aos="slide-left">
        <a href="https://medium.com/@josh.vazq" target="_blank" rel="noopener noreferrer">
          <i className="fab fa-medium " />
        </a>
      </li>
    </ul>
    <a href="https://www.contentful.com/" rel="noopener noreferrer nofollow" target="_blank">
      <img
        className="block my-2 w-64 mx-auto"
        src="https://images.ctfassets.net/fo9twyrwpveg/7F5pMEOhJ6Y2WukCa2cYws/398e290725ef2d3b3f0f5a73ae8401d6/PoweredByContentful_DarkBackground.svg"
        alt="Powered by Contentful"
      />
    </a>
  </div>
);
