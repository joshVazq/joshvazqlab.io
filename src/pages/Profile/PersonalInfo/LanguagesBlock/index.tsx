import React from 'react';

export const LanguagesBlock = () => (
  <div>
    <p className=" ">
      <b>
        <i className="fa fa-globe  mr-4 teal" />Languages
      </b>
    </p>
    <p>English</p>
    {/*     <div className="lg:bg-light-gray br-pill">
      <div className="br-pill bg-teal" />
    </div> */}
    <p>Spanish</p>
    <br />
  </div>
);
