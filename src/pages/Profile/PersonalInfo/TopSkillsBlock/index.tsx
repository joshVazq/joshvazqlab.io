import React from 'react';
import { Skill } from '../../../../model/skill';
import { sortSkill } from '../../../../utils';

type Props = {
  skills: Skill[];
};

const renderSkill = (skill: Skill) => {
  const percentage = `${skill.level}%`;
  return (
    <div key={skill.id}>
      <p>{skill.name}</p>
      <div className="lg:bg-light-gray br-pill ">
        <div
          className="px-4 py-1 text-center text-xs br-pill bg-teal"
          style={{ width: percentage }}>
          {percentage}
        </div>
      </div>
    </div>
  );
};

export const TopSkillsBlock = ({ skills }: Props) => (
  <div>
    <p className="">
      <b>
        <i className="fa fa-asterisk  mr-4 teal" />
        Top Skills
      </b>
    </p>
    {skills
      .sort(sortSkill)
      .slice(0, 2)
      .map(renderSkill)}
    <a href="#skills"> See more </a>

    <br />
  </div>
);
