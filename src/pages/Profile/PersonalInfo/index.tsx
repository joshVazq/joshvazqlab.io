import React from 'react';
import { Profile } from '../../../model/profile';

/* import { TopSkillsBlock } from './TopSkillsBlock';
import { LanguagesBlock } from './LanguagesBlock'; */

type Props = {
  profile: Profile;
};
export const PersonalInfo = ({ profile }: Props) => {
  const avatar = profile.avatar && (
    <img
      title={profile.avatar.title}
      alt={profile.avatar.description}
      src={profile.avatar.url}
      className="bg-ecru-white w-64 rounded-full mx-auto"
    />
  );
  const phone = profile.phone && profile.phone.toString();
  return (
    <article className="flex flex-col items-center lg:p-8 mx-auto mw1100px">
      <h2 className="my-8 text-center text-5xl font-hairline leading-tight teal">{profile.name}</h2>
      <div className="flex flex-col justify-center align-center lg:flex-row lg:border-t border-gray-200 lg:p-12">
        <div className="text-center lg:order-1  lg:flex lg:items-center" data-aos="zoom-in">
          {avatar}
        </div>
        {profile.about && (
          <div className="lg:order-0 m-8">
            <h3 className="text-center text-4xl  my-3 font-hairline teal">About me</h3>
            <div className="text-center">
              {profile.about.split('\n').map((sentence, index) => (
                <p key={'about-' + index}>{sentence}</p>
              ))}
            </div>
          </div>
        )}

        <div className="m-8 flex flex-col items-center lg:order-2 justify-center">
          <p className="w-56 my-4">
            <i className="fa fa-briefcase  mr-4  teal" />
            {profile.headline}
          </p>
          {/* <p className="w-56"><i className="fa fa-home  mr-4  teal"></i>London, UK</p> */}
          <p className="w-56 my-4">
            <a href={'mailto:' + profile.email}>
              <i className="fa fa-envelope  mr-4  teal" />
              {profile.email}
            </a>
          </p>
          {phone && (
            <p className="w-56 my-4">
              <a href={'tel:' + phone}>
                <i className="fa fa-phone  mr-4  teal" />
                {phone}
              </a>
            </p>
          )}

          {/* <TopSkillsBlock skills={profile.skills} />
        <LanguagesBlock /> */}
        </div>
      </div>
    </article>
  );
};
