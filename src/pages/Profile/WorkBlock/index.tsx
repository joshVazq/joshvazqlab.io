import React from 'react';
import Markdown from 'markdown-to-jsx';
import { Experience } from '../../../model/experience';
import { Link } from '../../../model/link';
import { sortByFromDesc } from '../../../utils';

type Props = {
  experience: Experience[];
};

function renderExperienceList(list: Experience[]) {
  return list.sort(sortByFromDesc).map((experience: Experience, i) => (
    <div key={experience.id} className="mb-8 flex flex-col lg:flex-row  lg:justify-between">
      <div className="lg:w-1/3">
        <h4 className="font-bold text-2xl lg:mt-8 text-gray-700">{experience.title}</h4>
        <h4 className="font-normal">
          <i className="fa fa-calendar  mr-4" />
          {experience.dates.toString()}
        </h4>
      </div>
      <div className="lg:w-3/5">
        <h4 className="font-bold mt-4 lg:mt-8">{experience.company}</h4>
        {experience.description && (
          <Markdown {...{ className: 'block' }} /* options={markDownOptions} */>
            {experience.description}
          </Markdown>
        )}
        {experience.links && (
          <div className="my-4">
            {experience.links.map((link: Link, index) => (
              <a
                key={experience.id + '-link-' + index}
                className="teal block mb-1"
                href={link.url}
                target="_blank">
                <i className="fa fa-link pr-1" />
                {link.url}
              </a>
            ))}
          </div>
        )}
      </div>
    </div>
  ));
}

export const WorkBlock = ({ experience }: Props) => (
  <div className="bb pb-8 px-16 b--moon-gray lg:mx-auto lg:w-11/12">
    <h3 className="teal text-2xl  my-3 font-hairline">
      <i className="fa fa-suitcase  mr-4 " />
      Work Experience
    </h3>
    {renderExperienceList(experience)}
  </div>
);
