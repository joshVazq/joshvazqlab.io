import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import { WorkBlock } from './index';
import { Experience } from '../../../model/experience';
import { Period } from '../../../model/period';
import { Link } from '../../../model/link';

describe('WorkBlock', () => {
  it('should render without crashing', () => {
    const exp = new Experience('');
    exp.dates = new Period(new Date());
    const experience = [exp];
    shallow(<WorkBlock experience={experience} />);
  });
  it('should render with description', () => {
    const exp = new Experience('');
    exp.dates = new Period(new Date());
    exp.description = 'description';
    const experience = [exp];
    shallow(<WorkBlock experience={experience} />);
  });
  it('should render with links', () => {
    const exp = new Experience('');
    exp.dates = new Period(new Date());
    exp.links = [new Link('name', 'url')];
    const experience = [exp];
    shallow(<WorkBlock experience={experience} />);
  });
});
