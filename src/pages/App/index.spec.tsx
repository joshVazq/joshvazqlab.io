import React from 'react';
import ReactDOM from 'react-dom';
import { App, mapStateToProps } from './index';
import { shallow } from 'enzyme';

import AOS from 'aos';
jest.mock('aos');

describe('App', () => {
  it('renders without crashing', () => {
    shallow(<App isLoading={true} />);
    expect(AOS.init).toHaveBeenCalled();
  });

  it('shoulg get props from mapStateToProps', () => {
    const props = mapStateToProps({ ui: { loading: true } });
    expect(props.isLoading).toBe(true);
  });
});
