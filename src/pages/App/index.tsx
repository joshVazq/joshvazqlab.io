import React from 'react';
import AOS from 'aos';

import { connect } from 'react-redux';
import { isLoading } from '../../redux/ui/selectors';
import ProfilePage from '../Profile';

/* import NavBar from "./NavBar";*/
import './style.css';
import 'aos/dist/aos.css';

/* type Props = {
  isLoading: boolean
}; */
export class App extends React.Component<{ isLoading: boolean }> {
  componentDidMount() {
    AOS.init();
  }
  render() {
    return (
      <div className="font-sans wow">
        {/* <NavBar /> */}
        <div>
          <ProfilePage />
        </div>
      </div>
    );
  }
}

export const mapStateToProps = (state: any) => ({
  isLoading: isLoading(state)
});

export default connect(mapStateToProps)(App);
