import { Period } from './model/period';
import { sortByFromAsc, sortByFromDesc, sortSkill } from './utils';
import { Skill } from './model/skill';

describe('Utils', () => {
  describe('sortByFromAsc ', () => {
    it(' should return the elements in the correct order', () => {
      const elements = [
        { dates: new Period(new Date('2015-03')) },
        { dates: new Period(new Date('2015-04')) },
        { dates: new Period(new Date('2015-01')) }
      ];
      const result = [...elements];
      result.sort(sortByFromAsc);
      expect(result).toEqual([elements[2], elements[0], elements[1]]);
    });
  });
  describe('sortByFromDesc ', () => {
    it(' should return the elements in the correct order', () => {
      const elements = [
        { dates: new Period(new Date('2015-03')) },
        { dates: new Period(new Date('2015-04')) },
        { dates: new Period(new Date('2015-01')) }
      ];
      const result = [...elements];
      result.sort(sortByFromDesc);
      expect(result).toEqual([elements[1], elements[0], elements[2]]);
    });
  });
  describe('sortSkill ', () => {
    it(' should return the elements in the correct order', () => {
      const elements = [
        new Skill('', 'one', 40),
        new Skill('', 'one', 60),
        new Skill('', 'one', 50)
      ];
      const result = [...elements];
      result.sort(sortSkill);
      expect(result).toEqual([elements[1], elements[2], elements[0]]);
    });
  });
});
